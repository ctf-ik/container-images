# Container images

[![pipeline status](https://gitlab.com/ctf-ik/container-images/badges/main/pipeline.svg)](https://gitlab.com/ctf-ik/container-images/-/commits/main)

## CTF Custom Images

Dockerfile design to package internal CTF-IK components

- Wireguard

## CTF Images

Dockerfile design to implement challenges CTF

- Python Flask
- Socat Bin
- Socat Python
- SSH
